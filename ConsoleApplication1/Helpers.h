#pragma once

#include <iostream>

int square_sum(int a, int b) {
    return (a + b) * (a + b);
}

#include "Helpers.h"

int main() {
    int a = 5, b = 10;

    std::cout << "Sum of squares of " << a << " and " << b << " is: " << square_sum(a, b) << std::endl;

    return 0;
}